import {
  appName,
  version,
  domain
} from '../../package'

const apiRoot = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:8888/bc-wemakesites-net/api' : `${domain}/api`
const artist = 'theophagyst'

const config = {
  domain,
  appName,
  version,
  apiRoot,
  dark: true,
  appIcon: 'mdi-bandcamp',
  weMakeSitesNet: 'https://wemakesites.net',
  payPalDonateLink: 'https://www.paypal.me/acidmartin',
  apiKey: '14804fe005220d42595e1c2adb65c658',
  googleAnalyticsId: 'G-CSXWL6SLK9',
  colors: {
    primary: 'blue-grey',
    links: 'blue-grey--text',
    accent: 'blue-grey darken-4',
    success: 'green',
    hex: '#607d8b'
  },
  methods: {
    artist: {
      artist
    },
    album: {
      artist,
      album: 'kinaesthetic-equilibrista'
    },
    track: {
      artist,
      track: 'gemini'
    },
    search: {
      q: 'death metal',
      page: 1
    },
    player: {
      album: 3639750531
    }
  }
}

export default config
