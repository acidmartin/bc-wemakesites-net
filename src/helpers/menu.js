import {
  album,
  artist,
  track,
  search,
  player
} from '../../api/routes'

const menu = [{
  label: 'Overview',
  url: '/',
  name: 'overview',
  icon: 'mdi-information-outline'
}, {
  label: 'Auth and API Key',
  url: '/auth',
  name: 'auth',
  icon: 'mdi-key-variant'
}, {
  label: 'Endpoint',
  url: '/pages/endpoint',
  name: 'endpoint',
  icon: 'mdi-lan-connect'
}, {
  label: 'Methods',
  url: '/methods',
  sub: [
    album,
    artist,
    player,
    search,
    track
  ],
  name: 'methods',
  icon: 'mdi-server'
}, {
  label: 'Demos',
  url: '/demos',
  name: 'demos',
  icon: 'mdi-settings'
}, {
  label: 'Donate',
  url: '/pages/donate',
  name: 'donate',
  icon: 'mdi-wallet-giftcard'
}]

export default menu
