/**
 * Email string validation rules
 * @module email
 */

import lang from './lang'

const email = [
  email => !!email || lang.required.toUpperCase(),
  email => /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email) || lang.invalidEmailFormat.toUpperCase()
]

export default email
