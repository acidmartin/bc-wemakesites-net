const footerLinks = [{
  label: 'Blog',
  url: 'https://martinivanov.net'
}, {
  label: 'Martin Ivanov',
  url: 'https://wemakesites.net'
}, {
  label: '@vuecidity',
  url: 'https://twitter.com/vuecidity'
}, {
  label: 'Theophagÿst',
  url: 'https://theophagyst.bandcamp.com'
}, {
  label: 'Service Status',
  url: 'https://bcstatus.wemakesites.net'
}]

export default footerLinks
