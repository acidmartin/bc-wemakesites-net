/**
 * String validation rules
 * @module value
 */

import lang from './lang'

const searchLength = [
  value => value.length >= 3 || lang.invalidSearch.toUpperCase()
]

export default searchLength
