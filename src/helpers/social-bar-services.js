/**
 * Social bar services
 * @module socialBarServices
 */

const socialBarServices = [{
  icon: 'twitter',
  url: (url) => {
    return `https://twitter.com/intent/tweet/?text=&url=${url}`
  },
  show: true
}, {
  icon: 'facebook',
  url: (url) => {
    return `https://facebook.com/sharer/sharer.php?u=${url}`
  },
  show: true
}, {
  icon: 'pinterest',
  url: (url) => {
    return `https://pinterest.com/pin/create/button/?url=${url}`
  },
  show: true
}, {
  icon: 'linkedin',
  url: (url) => {
    return `https://www.linkedin.com/shareArticle?mini=true&url=${url}`
  },
  show: true
}, {
  icon: 'xing',
  url: (url) => {
    return `https://www.xing.com/app/user?op=share;title=;url=${url}`
  },
  show: false
}, {
  icon: 'reddit',
  url: (url) => {
    return `https://reddit.com/submit/?url=${url}`
  },
  show: false
}, {
  icon: 'tumblr',
  url: (url) => {
    return `https://www.tumblr.com/widgets/share/tool?posttype=link&title=&caption=&shareSource=tumblr_share_button&content=${url}&canonicalUrl=${url}`
  },
  show: false
}, {
  icon: 'whatsapp',
  url: (url, siteTitle) => {
    return `whatsapp://send?text=${encodeURIComponent(siteTitle)} ${url}`
  },
  show: true
}, {
  icon: 'rss',
  url: () => {
    return `http://feeds.feedburner.com/acidmartin`
  },
  show: false
}, {
  icon: 'email',
  url: (url, siteTitle) => {
    return `mailto:?subject=${siteTitle}&body=${url}`
  },
  show: true
}]

export default socialBarServices
