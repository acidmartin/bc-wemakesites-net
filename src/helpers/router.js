import Vue from 'vue'
import Router from 'vue-router'
import PageIndex from '@/pages/index'
import StaticPage from '@/pages/static-page'
import MethodsPage from '@/pages/methods-page'
import AuthPage from '@/pages/auth'
import DemosPage from '@/pages/demos'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'page-index',
    component: PageIndex
  }, {
    path: '/auth',
    name: 'page-auth',
    component: AuthPage
  }, {
    path: '/demos',
    name: 'page-demos',
    component: DemosPage
  }, {
    path: '/pages/:page',
    name: 'static-page',
    component: StaticPage
  }, {
    path: '/methods',
    name: 'methods-main-page',
    component: MethodsPage
  }, {
    path: '/methods/:method',
    name: 'methods-page',
    component: MethodsPage
  }]
})
