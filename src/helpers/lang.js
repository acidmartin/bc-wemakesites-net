import toTitleCase from 'to-title-case'

const lang = {
  donate: 'Donate',
  email: 'Email',
  searchBandcamp: 'Search Bandcamp',
  header: 'Request Header',
  viewAlbum: 'View album',
  invalidSearch: 'Type three or more characters',
  backToDiscography: 'View discography',
  response: 'Response',
  yourApiKey: 'YOUR_API_KEY',
  viewOnBadcamp: 'Listen on Bandcamp',
  requestBody: '[POST] Request Body',
  invalidEmailFormat: 'Invalid email format',
  required: 'Required',
  cUrlExample: 'cURL Example',
  axiosExample: 'Axios Example',
  apiKeyInstructions: 'Type your email and submit the form, so we can create API key for you.',
  getApiKey: 'Get API Key',
  requestBodyInstructions: `The <code>album</code>, <code>artist</code> and <code>track</code> parameters of 
the request body should be the same as the URL slugs on Bandcamp, i.e:<br />
https://<span class="white--text">artist</span>.bandcamp.com/track/<span class="white--text">track</span><br />
https://<span class="white--text">artist</span>.bandcamp.com/album/<span class="white--text">album</span><br />`,
  noResults: (q) => {
    return `Your query for "${q}" did not yield any results. Please, try another search.`
  },
  method: (method) => {
    return `Methods${method ? ' &raquo; ' + toTitleCase(method) : ''}`
  },
  shareOn: (service) => {
    return `Share via ${service}`
  },
  searchSummary: (q, count) => {
    return `Your search for "${q}" yielded around ${count} results. Open your browser's console and click on a result item to check what data you can use for further actions.`
  },
  apiKeyGeneratedResponse: (key) => {
    return `<p>Success! Your API key is:</p>
<p><code class="headline">${key}</code></p>
<p>In order to use the API you need to pass it as <code>X-Api-Key</code> header value along with your requests.</p>
<p>We also sent you an email for future reference.</p>`
  }
}

export default lang
