import Vue from 'vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App'
import router from './helpers/router'
import VueGtag from 'vue-gtag'

import lang from './helpers/lang'
import config from './helpers/config'

import 'vuetify/dist/vuetify.min.css'

import BcArtist from './components/bc-artist'
import ApiKeyForm from './components/api-key-form'
import StaticPageContent from './components/static-page-content'
import SectionHeader from './components/section-header'
import CodeChunk from './components/code-chunk'
import SocialServices from './components/social-services'
import BcSearch from './components/bc-search'
import VcStyle from './components/vc-style'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueGtag, {
  config: {
    id: config.googleAnalyticsId,
    params: {
      send_page_view: false
    }
  }
}, router)

Vue.prototype.$lang = lang
Vue.prototype.$config = config

Vue.component('bc-artist', BcArtist)
Vue.component('api-key-form', ApiKeyForm)
Vue.component('static-page-content', StaticPageContent)
Vue.component('section-header', SectionHeader)
Vue.component('code-chunk', CodeChunk)
Vue.component('social-services', SocialServices)
Vue.component('bc-search', BcSearch)
Vue.component('vc-style', VcStyle)

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  components: { App },
  template: '<App/>',
  data () {
    return {
      config
    }
  },
  created () {
    this.axiosSetup()
    if (this.$config.dark) {
      document.querySelector('body').classList.add('body--theme--dark')
    }
  },
  methods: {
    axiosSetup () {
      const http = this.$http
      http.defaults.headers.common = {
        'X-Api-Key': this.$config.apiKey
      }
      http.interceptors.response.use(response => {
        window.scrollTo(0, 0)
        return response
      }, (error) => {
        return Promise.reject(error)
      })
    }
  }
})
