<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require './lib/PHPMailer/src/Exception.php';
require './lib/PHPMailer/src/PHPMailer.php';
require './lib/PHPMailer/src/SMTP.php';
include_once('./lib/dom-parser/dom-parser.php');

/**
 * @class Api
 * @author Martin Ivanov
 */
class Api {

  /**
   * SMTP/POP3 Settings
   */
  static $smtpPort = 587;
  static $smtpTransportLayer = 'tls';
  static $smtpServer = 'wemakesites.net';
  static $smtpUsername = 'admin@wemakesites.net';
  static $smtpPassword = 'cenobite1561';
  static $smtpDefaultToEmail = 'acid_martin@yahoo.com';
  static $fromName = 'BandCamp API';
  static $me = 'Martin Ivanov';

  /**
   * Public domain
   * @static
   */
  static $domain = 'https://bc.wemakesites.net';

  /**
   * Cache method responses
   * @static
   */
  static $cacheResponses = true;
  
  /**
   * Bandcamp embedded player
   * @static
   */
  static $embeddedPlayer = 'https://bandcamp.com/EmbeddedPlayer/v=2';

  /**
   * Keys data
   */
  static $keys = './data/api-keys.json';

  /**
   * Authorize requests by API key
   * @public auth
   */
  public function auth() {
    $keys = json_decode(file_get_contents(Api::$keys), true);
    if (@$header = getallheaders()['X-Api-Key']) {
      return in_array($header, $keys);
    }
  }

  /**
   * Flush cached responses
   * @public auth
   */
  public function flush() {
    $cacheDir = './cache/';
    foreach (array_slice(array_diff(scandir($cacheDir), array('..', '.', '.DS_Store', '.keep', '.htaccess')), 0 ) as $file) {
      unlink($cacheDir . $file);
    }
    echo json_encode(array(
      'status' => 'success',
      'code' => 0,
      'message' => 'cache flushed'
    ));
  }

  /**
   * Create md5 hash from a string
   * @public auth
   * @param $url string
   * @return string
   */
  public function hash ($url = '') {
    return md5($url);
  }

  /**
   * Cache a response from a method
   * @public cache
   * @param $data array
   * @return void
   */
  public function cache ($data) {
    if (Api::$cacheResponses) {
      $hash = $data['hash'];
      $hashFolder = './cache/';
      $file = $hashFolder . $hash . '.json';
      file_put_contents($file, json_encode($data));
    }
  }

  /**
   * Retrieve hashed response
   * @public retrieve
   * @param $hash string
   * @return string
   */
  public function retrieve ($hash) {
    $hashFolder = './cache/';
    $file = $hashFolder . $hash . '.json';
    if (file_exists($file)) {
      $data = json_decode(file_get_contents($file), true);
      $data['fromCache'] = true;
      Api::success('success', $data);
      die();
    }
  }

  /**
   * Search Bandcamp
   * @public search
   */
  public function search () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $payload = json_decode(file_get_contents('php://input'), true);
      $page = $payload['page'] ? $payload['page'] : 1;
      if (!$payload['q']) {
        $this -> error(5, 'Request body is missing "q" parameter');
        return;
      }
      $q = urlencode(strtolower($payload['q']));
      $url = 'https://bandcamp.com/search?page=' . $page . '&q=' . $q;
      if(@$html = file_get_html($url)) {
        $resultItems = @$html -> find('.result-items', 0);
        $results = array(
          'albums' => array(),
          'tracks' => array(),
          'artists' => array(),
          'fans' => array(),
          'labels' => array()
        );
        foreach ($resultItems -> find('li') as $li) {
          $itemType = trim(strtolower($li -> find('.itemtype', 0) -> plaintext));
          $type = $itemType . 's';
          $tags = trim($li -> find('.tags', 0) -> plaintext);
          $tags = preg_replace('!\s+!', ' ', $tags);
          $tags = str_replace(', ', ',', $tags);
          $tags = str_replace('tags: ', '', $tags);
          $tags = explode(',', $tags);
          $genres = trim($li -> find('.genre', 0) -> plaintext);
          $genres = preg_replace('!\s+!', ' ', $genres);
          $genres = str_replace(', ', ',', $genres);
          $genres = str_replace('genre: ', '', $genres);
          $genres = explode(',', $genres);
          $bandcampUrl = trim($li -> find('.itemurl', 0) -> plaintext);
          $result = array(
            'type' => substr_replace($type ,'',-1),
            'art' => $li -> find('.art img', 0) -> getAttribute('src'),
            'name' => trim($li -> find('.heading', 0) -> plaintext),
            'genres' => $genres[0] !== '' ? $genres : [],
            'bandcampUrl' => $bandcampUrl,
            'tags' => $tags
          );
          switch ($itemType) {
            case 'album':
              $result['albumSlug'] = explode('/album/', $bandcampUrl)[1];
              $result['artistSlug'] = str_replace('https://', '', explode('.', $bandcampUrl)[0]);
              break;
            case 'artist':
              $result['artistSlug'] = str_replace('https://', '', explode('.', $bandcampUrl)[0]);
              break;
            case 'label':
              $result['labelSlug'] = str_replace('https://', '', explode('.', $bandcampUrl)[0]);
              break;
            case 'track':
              $result['traclSlug'] = explode('/track/', $bandcampUrl)[1];
              $result['artistSlug'] = str_replace('https://', '', explode('.', $bandcampUrl)[0]);
              break;
            case 'fan':
              $result['fanSlug'] = str_replace('/', '', explode('.', $bandcampUrl)[2]);
              break;
          }
          $results[$type][] = $result;
        }
        $pages = sizeof(@$html -> find('.pagelist li'));
        $count = sizeof(@$html -> find('.result-items li'));
        $data = array(
          'page' => $page,
          'q' => $payload['q'],
          'qEncoded' => $q,
          'url' => $url,
          'results' => $results,
          'pages' => $pages,
          'count' => $count * $pages
        );
        Api::success('success', $data);
      } else {
        Api::error(5,'internal server error', null);
      }
    } else {
      Api::error();
    }
  }

  /**
   * Create API key
   * @public apikey
   * @return string
   */
  public function apikey () {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $file = Api::$keys;
      $payload = json_decode(file_get_contents('php://input'), true)['email'];
      $hash = md5($payload);
      $keys = json_decode(file_get_contents($file), true);
      if (in_array($hash, $keys)) {
        Api::success('api key exists', array(
          'key' => $hash
        ));
      } else {
        $keys[] = $hash;
        file_put_contents($file, json_encode($keys, JSON_PRETTY_PRINT));
        $fromName = Api::$fromName;
        $domain = Api::$domain;
        $myEmail = Api::$smtpDefaultToEmail;
        $subject = 'Your API Key';
        /**
         * Send to their
         */
        $contactFormMessageTemplate = '<p>Hello,</p>
<p>Thank you for registering for <a href="' . $domain . '">' . $fromName . '</a>! Your API key is:</p>
<p><strong>' . $hash . '</strong></p>
<p>In order to use it you should pass it as <strong>X-Api-Key</strong> header along with your requests, i.e:</p>
<p><code>\'X-Api-Key\': \'' . $hash . '\'</code></p>
<p>Please, keep this email for future reference as we do not store email data on our servers.</p>
<p>Kind regards,<br />' . $fromName . '<br /><a href="' . $domain . '">' . $domain . '</a></p>
';
        Api::email($fromName, $subject, Api::$smtpDefaultToEmail, Api::$me, $contactFormMessageTemplate, $payload);

        /**
         * Send to my email
         */
        $contactFormMessageTemplate = '<p>Hey there,</p>
<p>Somebody with email <a href="mailto:' . $payload . '">' . $payload . '</a> has registered for <a href="' . $domain . '">' . $fromName . '</a>! Their API key is:</p>
<p><strong>' . $hash . '</strong></p>
<p>Kind regards,<br />' . $fromName . '<br /><a href="' . $domain . '">' . $domain . '</a></p>
';
        Api::email($fromName, $subject, $myEmail, $myEmail, $contactFormMessageTemplate, $myEmail);
        Api::success('api key created', array(
          'key' => $hash
        ));
      }
    } else {
      Api::error();
    }
  }

  /**
   * Return auth error
   * @public unauthorized
   */
  public function unauthorized () {
    $this -> error(4, 'Not authorized');
    die();
  }

  /**
   * Get list of the available API methods
   * @method routes
   * @return string
   */
  public function routes () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
      $data = json_decode(file_get_contents('./routes.json'));
      Api::success('success', $data);
    } else {
      Api::error();
    }
  }

  /**
   * Get artist data by artist name
   * @method artist
   */
  public function artist () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $payload = json_decode(file_get_contents('php://input'), true);
      $url = $this -> makeUrl($payload['artist']);
      $hash = $this -> hash($url);
      $this -> retrieve($hash);
      if(@$html = file_get_html($url)) {
        $data = array();
        $data['title'] =  trim(@$html -> find('#band-name-location .title', 0) -> plaintext);
        if ($data['title']) {
          $data['url'] = $url;
          $data['artistSlug'] = $payload['artist'];
          $data['location'] =  trim(@$html -> find('#band-name-location .location', 0) -> plaintext);
          $data['photo'] =  @$html -> find('.band-photo', 0) -> getAttribute('src');
          $data['bio'] =  @$html -> find('meta[property="og:description"]', 0) -> getAttribute('content');
          $data['albums'] = array();
          $data['tracks'] = array();
          $data['links'] = array();
          $data['bio'] = preg_replace('!\s+!', ' ', $data['bio']);
          if(@$albums = $html -> find('.music-grid', 0)) {
            foreach($albums -> find('li') as $li) {
              $link = $li -> find('a', 0) -> getAttribute('href');
              $title = trim($li -> find('.title', 0) -> plaintext);
              if (preg_match('/album/', $link)) {
                $data['albums'][] = array(
                  'title' => $title,
                  'art' => $li -> find('img', 0) -> getAttribute('src'),
                  'slug' => str_replace('/album/', '', $link)
                );
              } else if (preg_match('/track/', $link)) {
                $data['tracks'][] = array(
                  'title' => $title,
                  'art' => $li -> find('img', 0) -> getAttribute('src'),
                  'slug' => str_replace('/track/', '', $link)
                );
              }
            }
          }
          if(@$links = $html -> find('#band-links', 0)) {
            foreach($links -> find('li') as $li) {
              $link = $li -> find('a', 0);
              $data['links'][] = array(
                'title' => trim($link -> plaintext),
                'url' => $link -> getAttribute('href')
              );
            }
          }
          $data['hash'] = $hash;
          $this -> cache($data);
          Api::success('success', $data);
        } else {
          Api::error(2,'artist not found', null);
        }
      } else {
        Api::error(2,'artist not found', null);
      }
    } else {
      Api::error();
    }
  }

  /**
   * Fet album streaming data to use with custom a custom player rather than embedding it
   * @public player
   */
  public function player () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $payload = json_decode(file_get_contents('php://input'), true);
      $album = $payload['album'];
      if (!$album) {
        $this -> error(6, 'Request body is missing "album" parameter');
        return;
      }
      $hash = $this -> hash($album);
      $this -> retrieve($hash);
      if(@$html = file_get_html('https://bandcamp.com/EmbeddedPlayer/album=' . $album)) {
        $playerdata = $html -> find('script[data-player-data]', 0) -> getAttribute('data-player-data');
        $playerdata = str_replace("&quot;", '"', $playerdata);
        $playerdata = json_decode($playerdata, true);
        $playerdata['hash'] = $hash;
        $this -> cache($playerdata);
        Api::success('success', $playerdata);
//        Api::success('success', $playerdata);
//        $scripts = $html -> find('script');
//        foreach ($scripts as $script) {
//          $playerdata = $script -> innertext;
//          $playerdata = trim($playerdata);
//          if (strpos($playerdata, 'playerdata')) {
//            $playerdata = explode(';', $playerdata);
//            $playerdata = str_replace('var playerdata = ', '', $playerdata[0]);
//            $playerdata = json_decode($playerdata, true);
//            $playerdata['hash'] = $hash;
//            $this -> cache($playerdata);
//            Api::success('success', $playerdata);
//          }
//        }
      }
    }
  }

  /**
   * Get album info by artist name and album slug
   * @method album
   * @return string
   */
  public function album () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $payload = json_decode(file_get_contents('php://input'), true);
      $url = $this -> makeUrl($payload['artist'], $payload['album']);
      $hash = $this -> hash($url);
      $this -> retrieve($hash);
      if(@$html = file_get_html($url)) {
        $data = array();
        $player = Api::$embeddedPlayer;
        $embed = @$html -> find('meta[property="og:video"]', 0) -> getAttribute('content');
        $embed = str_replace($player, '', $embed);
        $embedParams = explode('/', $embed);
        $finalEmbedParams = array();
        $finalEmbedParams[] = array(
          'uri' => $player
        );
        foreach ($embedParams as $embedParam) {
          $pair = explode('=', $embedParam);
          if ($pair[0] && $pair[1]) {
            $finalEmbedParams[] = array(
              $pair[0] => $pair[1]
            );
          }
        }
        $data['url'] = $url;
        $data['albumSlug'] = $payload['album'];
        $data['artistSlug'] = $payload['artist'];
        $data['tracks'] = array();
        $data['embed'] = $player . $embed;
        $data['embedParams'] = $finalEmbedParams;
        $data['artist'] = trim(@$html -> find('#band-name-location .title', 0) -> plaintext);
        $data['title'] = trim(@$html -> find('.trackTitle', 0) -> plaintext);
        $data['location'] =  trim(@$html -> find('#band-name-location .location', 0) -> plaintext);
        $data['photo'] =  @$html -> find('.band-photo', 0) -> getAttribute('src');
        $data['art'] =  @$html -> find('.popupImage img', 0) -> getAttribute('src');
        $data['about'] =  trim(@$html -> find('.tralbum-about', 0) -> plaintext);
        $data['credits'] =  trim(@$html -> find('.tralbum-credits', 0) -> plaintext);
        $data['about'] = preg_replace('!\s+!', ' ', $data['about']);
        $data['credits'] = preg_replace('!\s+!', ' ', $data['credits']);
        if(@$tracks = $html -> find('.track_list', 0)) {
          foreach($tracks -> find('tr') as $tr) {
            $track = $tr -> find('.title a', 0);
            $length = trim($tr -> find('.time', 0) -> plaintext);
            $data['tracks'][] = array(
              'slug' => str_replace('/track/', '', $track -> getAttribute('href')),
              'title' => trim($track -> plaintext),
              'length' => $length
            );
          }
        }
        $data['hash'] = $hash;
        $this -> cache($data);
        Api::success('success', $data);
      } else {
        Api::error(2,'album not found', null);
      }
    } else {
      Api::error();
    }
  }

  /**
   * Get track info by artist name and track slug
   * @method track
   * @return string
   * }
   */
  public function track () {
    if (!$this -> auth()) {
      $this -> unauthorized();
    }
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $payload = json_decode(file_get_contents('php://input'), true);
      $url = $this -> makeUrl($payload['artist'], '', $payload['track']);
      $hash = $this -> hash($url);
      $this -> retrieve($hash);
      if(@$html = file_get_html($url)) {
        $data = array();
        $player = Api::$embeddedPlayer;
        $embed = @$html -> find('meta[property="og:video"]', 0) -> getAttribute('content');
        $embed = str_replace($player, '', $embed);
        $embedParams = explode('/', $embed);
        $finalEmbedParams = array();
        $finalEmbedParams[] = array(
          'uri' => $player
        );
        foreach ($embedParams as $embedParam) {
          $pair = explode('=', $embedParam);
          if ($pair[0] && $pair[1]) {
            $finalEmbedParams[] = array(
              $pair[0] => $pair[1]
            );
          }
        }
        $data['url'] = $url;
        $data['album'] = array(
          'title' => trim(@$html -> find('.fromAlbum', 0) -> plaintext),
          'slug' => str_replace('/album/', '', @$html -> find('.albumTitle a', 0) -> getAttribute('href'))
        );
        $data['embed'] = @$html -> find('meta[property="og:video"]', 0) -> getAttribute('content');
        $data['embedParams'] = $finalEmbedParams;
        $data['artistSlug'] = $payload['artist'];
        $data['trackSlug'] = $payload['track'];
        $data['artist'] = trim(@$html -> find('#band-name-location .title', 0) -> plaintext);
        $data['title'] = trim(@$html -> find('.trackTitle', 0) -> plaintext);
        $data['photo'] =  @$html -> find('.band-photo', 0) -> getAttribute('src');
        $data['art'] =  @$html -> find('.popupImage img', 0) -> getAttribute('src');
        $data['about'] =  trim(@$html -> find('.tralbum-about', 0) -> plaintext);
        $data['credits'] =  trim(@$html -> find('.tralbum-credits', 0) -> plaintext);
        $data['about'] = preg_replace('!\s+!', ' ', $data['about']);
        $data['credits'] = preg_replace('!\s+!', ' ', $data['credits']);
        $data['hash'] = $hash;
        $this -> cache($data);
        Api::success('success', $data);
      } else {
        Api::error(3,'track not found', null);
      }
    } else {
      Api::error();
    }
  }

  /**
   * Return success status
   * @method success
   * @param $message string
   * @param $data object
   * @return string
   * }
   */
  public function success ($message = 'success', $data = null) {
    echo json_encode(array(
      'code' => 0,
      'status' => 'success',
      'message' => $message,
      'data' => $data
    ));
  }

  /**
   * Return error status
   * @method error
   * @param $code int
   * @param $message string
   * @param $data object
   * @return string
   * }
   */
  public function error ($code = 1, $message = 'method not allowed', $data = null) {
    echo json_encode(array(
      'code' => $code,
      'status' => 'fail',
      'message' => $message,
      'data' => $data
    ));
  }

  /**
   * Create BandCamp URL
   * @method makeUrl
   * @param $artist string
   * @param $album string
   * @param $track object
   * @return string
   * }
   */
  public function makeUrl ($artist = null, $album = null, $track = null) {
    if ($artist && !$album && !$track) {
      return 'https://' . $artist . '.bandcamp.com/';
    } else if ($artist && $album && !$track) {
      return 'https://' . $artist . '.bandcamp.com/album/' . $album;
    } else if ($artist && $track) {
      return 'https://' . $artist . '.bandcamp.com/track/' . $track;
    }
  }

  /**
   * Generic send email function
   * @static email
   * @param string $emailFromName
   * @param string $emailSubject
   * @param string $emailReplyTo
   * @param string $emailReplyToName
   * @param string $emailBody
   * @param string $emailTo
   * @return string
   */
  static function email ($emailFromName, $emailSubject, $emailReplyTo, $emailReplyToName, $emailBody, $emailTo) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $mail = new PHPMailer(true);
      $mail -> SMTPDebug = 0;
      $mail -> isSMTP();
      $mail -> Host = Api::$smtpServer;
      $mail -> SMTPAuth = true;
      $mail -> Username = Api::$smtpUsername;
      $mail -> Password = Api::$smtpPassword;
      $mail -> SMTPSecure = Api::$smtpTransportLayer;
      $mail -> Port = Api::$smtpPort;
      $mail -> setFrom(Api::$smtpUsername, $emailFromName);
      $mail -> addReplyTo($emailReplyTo, $emailReplyToName);
      $mail -> isHTML(true);
      $mail -> Subject = $emailSubject;
      $mail -> Body = $emailBody;
      $mail -> AltBody = strip_tags($emailBody);
      $mail -> addAddress($emailTo);
      $mail -> send();
    } else {
      Api::error();
    }
  }
}