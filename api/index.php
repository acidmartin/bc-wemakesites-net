<?php
/**
 * API routes
 */
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, OPTIONS, GET');
header('Access-Control-Allow-Headers: Origin, X-Custom-Header, X-Api-Key, X-Requested-With, Content-Type, Accept, Authorization');

ob_start('ob_gzhandler');

$request = $_SERVER['REQUEST_URI'];
$requestHost = $_SERVER['HTTP_HOST'];

require_once 'api.php';
$routes = json_decode(file_get_contents('./routes.json'), true);
$apiRoot = '/api/';

if (strpos($requestHost, 'localhost') !== false) {
    $apiRoot = '/bc-wemakesites-net/api/';
}

if ($request === $apiRoot) {
  die(json_encode(array(
    'status' => 'success'
  )));
}

$api = new Api();

switch ($request) {
  case $apiRoot . $routes['artist']['url']:
      $api -> artist();
      break;
  case $apiRoot . $routes['albums']['url']:
    $api -> albums();
    break;
  case $apiRoot . $routes['album']['url']:
    $api -> album();
    break;
  case $apiRoot . $routes['track']['url']:
    $api -> track();
    break;
  case $apiRoot . $routes['routes']['url']:
    $api -> routes();
    break;
  case $apiRoot . $routes['apikey']['url']:
    $api -> apikey();
    break;
  case $apiRoot . $routes['search']['url']:
    $api -> search();
    break;
  case $apiRoot . $routes['flush']['url']:
    $api -> flush();
    break;
  case $apiRoot . $routes['player']['url']:
    $api -> player();
    break;
}