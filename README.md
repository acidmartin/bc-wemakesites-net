# bc-wemakesites-net

> RESTFul API for Bandcamp by Martin Ivanov

## Cronjob for cache flushing

> 0	*	*	*	*	/opt/cpanel/ea-php72/root/usr/bin/php /home/wemakesites/bc.wemakesites.net/api/delete-cache-curl.php >/dev/null 2>&1

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
